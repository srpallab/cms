from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def userProfile(request):
    context = {}
    return render(request, "accounts/profile.html", context)
