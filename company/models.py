from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Info(models.Model):
    name = models.CharField(max_length=30)
    address = models.TextField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=30)
    code = models.CharField(max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.ForeignKey(Info, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
