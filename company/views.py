from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from . import models


@login_required
def index(request):
    allDeptObjs = models.Department.objects.all()
    context = {"allDeptObjs": allDeptObjs}
    return render(request, "company/dashboard.html", context)
