from django.contrib import admin
from .models import Info, Department

admin.site.register(Info)
admin.site.register(Department)
