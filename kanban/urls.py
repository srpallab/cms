from django.urls import path
from . import views

urlpatterns = [
    path("addtask/", views.createTask, name="addtask"),
    path("updatetask/<pk>", views.updateTask, name="updatetask"),
    path("deletetask/<pk>", views.deleteTask, name="deletetask"),
]
