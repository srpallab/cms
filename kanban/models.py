from django.db import models
from django.contrib.auth.models import User
from company.models import Department

# Create your models here.


class Task(models.Model):
    title = models.CharField(max_length=180)
    description = models.TextField(max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    ended_at = models.DateTimeField()
