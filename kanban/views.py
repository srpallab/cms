from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from . import forms, models
from datetime import datetime


@login_required
def createTask(request):
    if request.POST:
        addform = forms.AddTask(request.POST)
        if addform.is_valid():
            endTime = datetime.combine(
                addform.cleaned_data["date"], addform.cleaned_data["time"]
            )
            # print(endTime)
            newTask = models.Task(
                title=addform.cleaned_data["title"],
                description=addform.cleaned_data["description"],
                user=request.user,
                department=addform.cleaned_data["department"],
                ended_at=endTime,
            )
            newTask.save()
            print(newTask)
            return redirect("home")

    context = {
        "taskForm": forms.AddTask,
        "mode": "addtask",
        "actionName": "Add Task",
    }
    return render(request, "kanban/create_and_update_task.html", context)


@login_required
def updateTask(request, pk):
    taskObj = models.Task.objects.get(pk=pk)
    context = {
        "taskForm": forms.AddTask(
            initial={
                "title": taskObj.title,
                "description": taskObj.description,
                "department": taskObj.department,
                "date": taskObj.ended_at.date,
                "time": taskObj.ended_at.time,
            }
        ),
        "mode": "updatetask",
        "pk": pk,
        "actionName": "Update Task",
    }
    if request.POST:
        updatedform = forms.AddTask(request.POST)
        if updatedform.is_valid():
            endTime = datetime.combine(
                updatedform.cleaned_data["date"],
                updatedform.cleaned_data["time"],
            )
            taskObj.title = updatedform.cleaned_data["title"]
            taskObj.description = updatedform.cleaned_data["description"]
            taskObj.department = updatedform.cleaned_data["department"]
            taskObj.ended_at = endTime
            taskObj.save()
            return redirect("home")
    return render(request, "kanban/create_and_update_task.html", context)


@login_required
def deleteTask(request, pk):
    models.Task.objects.get(pk=pk).delete()
    context = {"taskForm": forms.AddTask}
    return redirect("home")
