from django import forms
from . import models


class AddTask(forms.Form):
    title = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    department = forms.ModelChoiceField(models.Department.objects.all())
    date = forms.DateField(widget=forms.DateInput(attrs={"type": "date"}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={"type": "time"}))
